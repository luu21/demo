#Exercise 1 :
u, v, x, y, z = 29, 12, 10, 4, 3
u / v
t = (u == v)
print(t)
u % v
t = (x >= y)
print(t)
u += 5
print(u)
u %=z
print(u)
t = (v > x and y < z)
print(t)
x**z
x // z


# Exercise 2
# Write a python script to compute the perimeter and the area of a circle with radius r = 5.

import math

r = 5
p = r * 2 * math.pi
print("the perimeter of a circle with radius r = 5 is " + str(p))
a = r ** 2 * math.pi
print("the area of a circle with radius r = 5 is " + str(a))

# Given a string s = “Hi John, welcome to python programming for beginner!”. Your mission is write a small python script to:
s = "Hi John, welcome to python programming for beginner!"
print(s)
# Check a string “python” that either exists in s or not.
t = "python" in s
print(t)
# Extract the word “John” from s and save it into a variable named s1.
s1 = s.split(" ")[1].strip(",")
print(s1)
# Count how many character ‘o’ in s and print it on console. Guide: use count() function of string.
count = s.count('o')
print(count)
# Count how many word in s and print it on console.
li = s.split(" ")
print(len(li))


# Exercise 3 : Write a python script to print the following string in a specific format. s = “Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are”.  The output is:
# Twinkle, twinkle, little star,
# How I wonder what you are!
# Up above the world so high,
# Like a diamond in the sky.
# Twinkle, twinkle, little star,
# How I wonder what you are.
#
# Exercise 4 : Given a list as follows: l = [23, 4.3, 4.2, 31, ‘python’, 1, 5.3, 9, 1.7]
# Remove the item “python” in the list.
l = [23, 4.3, 4.2, 31, "python", 1, 5.3, 9, 1.7]
print(l)
l.remove('python')
# Sort this list by ascending
print(sorted(l))
# Sort this list by descending.
print(sorted(l, reverse=True))
# Check either number 4.2 to be in l or not?
t = 4.2 in l
print(t)

# Exercise 5 : Sets
#
# Write a python script to complete the following tasks:
# Create three sets: A = {1, 2, 3, 4, 5, 7}, B = {2, 4, 5, 9, 12, 24}, C = {2, 4, 8}
A = {1, 2, 3, 4, 5, 7}
B = {2, 4, 5, 9, 12, 24}
C = {2, 4, 8}
# Iterate over all elements of set C and add each element to A and B
for i in C:
    A.add(i)
    B.add(i)

# Print out A and B after adding elements
print(A)
print(B)

# Print out the intersection of A and B
print(A.intersection(B))
# Print out the union of A and B
print(A.union(B))
# Print out elements in A but not in B
print(A.difference(B))
# Print out the length of A and B
print('the length of A is: ' + str(len(A)))
# Print out the maximum value of A union B
print(max(A.union(B)))
# Print out the minimum value of A union B
print(min(A.union(B)))

#exercise 7 : Tuples

# Write a python script to:
# Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
t = (1, 'python', [2, 3], (4, 5))
print(t)
# Unpack t into 6 variables, that are: 1, 'python', 2, 3, 4, 5

# Print out the last element of t
print(t[-1])
# Add to t a list [2, 3]
t = t + ([2, 3],)
print(t)

# Check whether list [2, 3] is duplicated in t

# Remove list [2, 3] from t

t = t - ([2, 3],)
# Convert tuple t into a list
t = list(t)

# Exercise 8 : Dictionaries

# Write a Python script to concatenate following dictionaries to create a new one.
dic1 ={1 : 10, 2 : 20}
dic2 ={3 : 30, 4 : 40}
dic3 ={5 : 50, 6 : 60}
# Expected result: {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}

dic = {**dic1, **dic2, **dic3}  # available from Python 3.5, equal to copy and then update
# Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included)
# and the values are square of keys.
key = list(range(1, 16))
values = list()
for i in key:
    values.append(i**2)
print(values)
dict(zip(key, values))
# Write a Python script to sort ascending a dictionary by value. For example,
# input: {'a': 1, 'b': 4, 'c': 2}
# ouput: ['a', 'c', 'b']
input = {'a': 1, 'b': 4, 'c': 2}
# Input is a dictionary. Output is a list of key where the order are the order of their mapped values.


# exercise 9 : Given a string "Python is an easy language to learn". Write a script to create a dictionary where the keys are unique characters of the string and the mapped values are their occurrence in the string. For example, the expected output for the above string is:
# {'P': 1, 'a': 5, 'e': 3, 'g': 2, 'h': 1, 'i': 1, 'l': 2, 'n': 4, 'o': 2, 'r': 1, 's': 2, 't': 2, 'u': 1, 'y': 2}

string = "Python is an easy language to lear"
key = set(list(string))
key.remove(' ')
values = list()
for i in key:
    values.append(string.count(i))
dic = dict(zip(key, values))